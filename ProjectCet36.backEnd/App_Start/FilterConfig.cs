﻿using System.Web;
using System.Web.Mvc;

namespace ProjectCet36.backEnd
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
